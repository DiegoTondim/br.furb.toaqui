var pictureSource; // picture source
var destinationType; // sets the format of returned value
var map;
var marker;
var infowindow;
var watchID;

audioAtivo = true;
beacons = {};
avisos = {};
cacheSalas = {};
eventos = null;
salas = null;
		
$(function(){			
	app.initialize();
				
	$("#btnAudio").click(function(){
		if(audioAtivo){
			audioAtivo = false;
			$(this).removeClass("ativo");
			$(this).addClass("inativo");
		} else {
			audioAtivo = true;
			$(this).removeClass("inativo");
			$(this).addClass("ativo");
		}
	});			
	
	$('#eventos').listview();
	
	try {
		eventos = new Eventos();
		salas =  new Salas();
		ko.applyBindings(eventos, $("#left-panel")[0]);
		ko.applyBindings(salas, $("#right-panel")[0]);
	}catch(e){
		alert(e.message);
	}

	document.addEventListener("deviceready", onDeviceReady, false);
});

function onDeviceReady() {
	cordova.plugins.notification.local.schedule({
		id: 1,
		title: "Production Jour fixe",
		text: "Duration 1h",
		data: { meetingId:"123#fg8" }
	});

	cordova.plugins.notification.local.on("click", function (notification) {
		joinMeeting(notification.data.meetingId);
	});
	
	db = window.openDatabase("TOAQUI", "1.0", "TOAQUI", 200000);
	
	try {
		pictureSource=navigator.camera.PictureSourceType;
		destinationType=navigator.camera.DestinationType;
		
		navigator.geolocation.getCurrentPosition(onSuccessLoadMap, onErrorLoadMap);
	}catch(ms){
		alert("Erro ao iniciar o Bluetooth. \r\n" + ms.message);
	}
	
	window.bluetooth.isEnabled(isEnabledSuccess, isEnabledError);
	searchBeacon();
	//inicialização do banco de dados padrão
	 /*db.transaction(populateDB, errorCB, successCB);
	 db.transaction(function(tx){
			 tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR) VALUES ("BEC26202-A8D8-4A94-80FC-9AC1DE37DAA5", 0, 0, 2)');
			 tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("227", 1, 2)');
			 tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("215", 1, 2)');
			 tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA, AMBIENTE) VALUES ("Aula de Fundamentos da Computação", "Descrição da aula", NULL, 2)');
	 }, errorCB);*/
	db.transaction(buscarBeaconsIniciarMonitoramento, errorCB);
	
	ttsPlugin.initTTS(function(){
		ttsPlugin.setRate("0.2"); // Set voice speed : default is "0.2"
		ttsPlugin.setLanguage("pt-BR"); // Set voice language : default is "en-US"

		//ttsPlugin.stopSpeaking(); // Try to stop speaking
	}, function(e){
		alert("Não foi possível iniciar o plugin de síntese de voz");
	}); // Init Plugin : failCallBack doesn't work yes
	
	setInterval(function(){
		try {
			if(!audioAtivo)
				return;
				
			var mensagem = "";
			
			for(i = 0; i < eventos.quantidade(); i++){
				var virgula = "";
				if(eventos.quantidade() > 1 && i+2 < eventos.quantidade())
					virgula = ",";
				else if(eventos.quantidade() > 1 && i+2 == eventos.quantidade())
					virgula = " e ";
					
				var e = eventos.obter(i);
				mensagem += e.nome() + virgula;
				if(eventos.quantidade() == 1)
					mensagem += " está sendo realizado em " + e.ambiente().bloco() + " " + e.ambiente().descricao();
				avisos[e.id()] = new Date();
			}
			if(eventos.quantidade() > 1)
				mensagem += " estão acontecendo próximos à você";
			
			ttsPlugin.speak(mensagem);
			ttsPlugin.stopSpeaking();
		} catch(e){
			alert("Erro ao executar a síntese de voz. \r\n " + e.message);
		}
	}, 60000);
}

$( document ).on( "pagecreate", "#demo-page", function() {
    $( document ).on( "swipeleft swiperight", "#demo-page", function( e ) {
        // We check if there is no open panel on the page because otherwise
        // a swipe to close the left panel would also open the right panel (and v.v.).
        // We do this by checking the data that the framework stores on the page element (panel: open).
        if ( $( ".ui-page-active" ).jqmData( "panel" ) !== "open" ) {
            if ( e.type === "swipeleft" ) {
                $( "#right-panel" ).panel( "open" );
            } else if ( e.type === "swiperight" ) {
                $( "#left-panel" ).panel( "open" );
            }
        }
    });
});

function onErrorLoadMap(error){
	alert("Erro ao carregar o mapa " + error.code + ". \n" + "" + error.message);
};

function onSuccessLoadMap(position){
	var longitude = position.coords.longitude;
	var latitude = position.coords.latitude;
	var latLong = new google.maps.LatLng(latitude, longitude);

	var mapOptions = {
		center: latLong,
		zoom: 60,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	var map = new google.maps.Map(document.getElementById("map"), mapOptions);

	var marker = new google.maps.Marker({
		  position: latLong,
		  map: map,
		  title: 'my location'
	});
};

function logToDom(message) {
	var e = document.createElement('label');
	e.innerText = message;
	var br = document.createElement('br');
	var br2 = document.createElement('br');
	document.body.appendChild(e);
	document.body.appendChild(br);
	document.body.appendChild(br2);
}

function isEnabledSuccess(isEnabled){
   if(!isEnabled)
	 enableBluetooth();
}

function isEnabledError(error){
}

function enableBluetooth(){
	window.bluetooth.enable( function(){}, function(){});
}

function createBeacon(uuid, identifier)
{
	var beacon = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid);
	return beacon;   
}   

function searchBeacon() {
   cordova.plugins.locationManager.requestWhenInUseAuthorization();
   var delegate = new cordova.plugins.locationManager.Delegate();
   avisos["noevent"] = new Date();
   //delegate.implement({
	delegate.didDetermineStateForRegion = function (pluginResult) {
		//logToDom('[DOM] didDetermineStateForRegion: ' + JSON.stringify(pluginResult));
		//cordova.plugins.locationManager.appendToDeviceLog('[DOM] didDetermineStateForRegion: '
		//    + JSON.stringify(pluginResult));
	};
	delegate.didStartMonitoringForRegion = function (pluginResult) {
		//console.log('didStartMonitoringForRegion:', pluginResult);
		//logToDom('didStartMonitoringForRegion:' + JSON.stringify(pluginResult));
	};
		
	delegate.didRangeBeaconsInRegion = function (pluginResult) {
		var jsonData = JSON.stringify(pluginResult);
		var jsonPData = JSON.parse(jsonData);
	
		//logToDom('[DOM] didRangeBeaconsInRegion: ' + JSON.stringify(pluginResult));
	
		var distancia = -1;
		$.each(jsonPData.beacons, function(index, item){
			distancia = item.accuracy;
		});
		
		db.transaction(function(tx){
			if(jsonPData.beacons.length == 0 && cacheSalas[jsonPData.region.identifier] != undefined && Math.abs(new Date() - cacheSalas[jsonPData.region.identifier]) > 15000){
				salas.removerPorBeacon(jsonPData.region.identifier);
			} else {
				cacheSalas[jsonPData.region.identifier] = new Date();
				tx.executeSql('SELECT A.ID AS AID, A.DESCRICAO AS ADESCRICAO, A.BEACON AS ABEACON, B.DESCRICAO AS BLOCO ' + 
							' FROM AMBIENTE A INNER JOIN BLOCO B ON B.ID = A.BLOCO WHERE A.BEACON = ' + jsonPData.region.identifier, [], function(txt, result){
					for(var i = 0; i < result.rows.length; i++) {
						try {
							var row = result.rows.item(i);
							var a = new Ambiente();
							a.id(row["AID"]);
							a.descricao(row["ADESCRICAO"]);
							a.beacon(row["ABEACON"]);
							a.bloco(row["BLOCO"]);
							
							salas.adicionar(a);
						} catch(ex){
							alert("Erro ao adicionar ambiente. " + ex.message);
						}
					}
						
				}, function(ex){
					alert("Erro de SQL ao carregar ambientes. "+ex.message);
				});
			}
		}, errorCB);
		
		db.transaction(function(tx){
			tx.executeSql('SELECT E.*, A.ID AS AID, A.DESCRICAO AS ADESCRICAO, A.BEACON AS ABEACON, B.DESCRICAO AS BLOCO ' + 
							' FROM EVENTO E INNER JOIN AMBIENTE A ON A.ID = E.AMBIENTE INNER JOIN BLOCO B ON B.ID = A.BLOCO WHERE A.BEACON = ' + jsonPData.region.identifier, [], function(txt, result){
					for(var i = 0; i < result.rows.length; i++) {
						try {
							var row = result.rows.item(i);
							var e = new Evento(row["ID"], row["NOME"], row["DESCRICAO"], row["DATA"], row["AMBIENTE"]);
							var a = new Ambiente();
							a.id(row["AID"]);
							a.descricao(row["ADESCRICAO"]);
							a.beacon(beacons[row["ABEACON"]]);
							a.bloco(row["BLOCO"]);
							e.ambiente(a);
							if(distancia > -1){
								if((jsonPData.beacons.length == 0 && avisos[e.id()] != undefined && Math.abs(new Date() - avisos[e.id()]) > 30000) || distancia > 2.0){
									procurarEventoAtualizar(e, true);
								}
								else {
									procurarEventoAtualizar(e, false);
								}
							}
						} catch(ex){
							alert("Erro ao atualizar os eventos. " + ex.message);
						}
					}
					
			}, function(ex){
				alert("Erro de SQL ao carregar eventos. "+ex.message);
			});
		}, errorCB);
	};
	
	cordova.plugins.locationManager.setDelegate(delegate);
}
 
function procurarEventoAtualizar(evento, remover){
	if(remover){
		eventos.remover(evento);
	}
	else {
		eventos.adicionar(evento);
	}
	avisos[evento.id()] = new Date();
}