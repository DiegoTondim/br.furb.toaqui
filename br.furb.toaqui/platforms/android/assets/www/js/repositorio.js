function nullHandler(){}
			
//create table and insert some record
function populateDB(tx) {
	tx.executeSql('CREATE TABLE IF NOT EXISTS BEACON (ID INTEGER PRIMARY KEY, UID TEXT NOT NULL, LATITUDE FLOAT NOT NULL, LONGITUDE FLOAT NOT NULL, ANDAR INT NOT NULL)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS BLOCO (ID INTEGER PRIMARY KEY, DESCRICAO CHARACTER(1) NOT NULL)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS AMBIENTE (ID INTEGER PRIMARY KEY, DESCRICAO TEXT NOT NULL, BLOCO INT NOT NULL, BEACON INT NOT NULL, FOREIGN KEY (BEACON) REFERENCES BEACON(ID), FOREIGN KEY (BLOCO) REFERENCES BLOCO(ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS EVENTO (ID INTEGER PRIMARY KEY, NOME VARCHAR(150) NOT NULL, DESCRICAO TEXT NOT NULL, DATA DATETIME NULL, AMBIENTE INTEGER NOT NULL, FOREIGN KEY (AMBIENTE) REFERENCES AMBIENTE(ID))');
	tx.executeSql('INSERT INTO BLOCO (DESCRICAO) VALUES ("A")');
	tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR) VALUES ("BEC26202-A8D8-4A94-80FC-9AC1DE37DAA6", 0, 0, 1)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("210", 1, 1)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA, AMBIENTE) VALUES ("PALESTRA 1", "DESCRIÇÃO DA PALESTRA 1", NULL, 1)');
}

//function will be called when an error occurred
function errorCB(err) {
	alert("ERRO DE SQL: " + err.message + err.code);
}

//function will be called when process succeed
function successCB() {
	alert("SUCESSO!");
	db.transaction(buscarBeaconsIniciarMonitoramento,errorCB);
}

function buscarBeaconsIniciarMonitoramento(tx){
//adicionar os beacons em memória e cria os listeners
	tx.executeSql('SELECT * FROM BEACON', [], function (tx,result){
		if (result.rows.length > 0) {
			for(var i=0; i<result.rows.length; i++) {
				var row = result.rows.item(i);
				try {
					var beaconViewModel = new Beacon(row['ID'], row['UID'], row['LATITUDE'], row['LONGITUDE'], row['ANDAR']);
					beacons[beaconViewModel.id()] = beaconViewModel;
					var beacon = createBeacon(beaconViewModel.uid(), beaconViewModel.id());
					var beaconMoniter = cordova.plugins.locationManager.startMonitoringForRegion(beacon);
					var beaconRange = cordova.plugins.locationManager.startRangingBeaconsInRegion(beacon);
				}catch(e){
					alert(e.message);
				}
			}
		}
	}, function(e){alert("erro a "+e.message);});
}