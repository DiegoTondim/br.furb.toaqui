function Eventos() {
	var self = this;
	
	self.eventos = ko.observableArray([]);
	
	self.obter = function(index){
		return self.eventos()[index];
	};
	self.adicionar = function(evento){
		existe = false;
		for(i = 0; i < self.quantidade(); i++){
			if(self.eventos()[i].id() == evento.id()){
				existe = true;
				break;
			}
		}
		if(!existe)
			self.eventos.push(evento);
	};
	self.remover = function(evento){
		self.eventos.remove(function(item) { 
			return item.id() == evento.id();
		});
	};
	self.quantidade = function(){
		return self.eventos().length;
	};
}

function Salas(eventos) {
	var self = this;
	
	self.salas = ko.observableArray([]);
	
	self.obter = function(index){
		return self.salas()[index];
	};
	self.adicionar = function(sala){
		existe = false;
		for(i = 0; i < self.quantidade(); i++){
			if(self.salas()[i].id() == sala.id()){
				existe = true;
				break;
			}
		}
		if(!existe)
			self.salas.push(sala);
	};
	self.removerPorBeacon = function(beacon){
		self.salas.remove(function(item) { 
			return item.beacon() == beacon;
		});
	};
	self.remover = function(sala){
		self.salas.remove(function(item) { 
			return item.id() == sala.id();
		});
	};
	self.quantidade = function(){
		return self.salas().length;
	};
}

function Beacon(id, uid, latitude, longitude, andar) {
	var self = this;
	
	self.id = ko.observable(""+id);
	self.uid = ko.observable(uid);
	self.latitude = ko.observable(latitude);
	self.longitude = ko.observable(longitude);
	self.andar = ko.observable(andar);
}

function Ambiente() {
	var self = this;
	
	self.id = ko.observable();
	self.descricao = ko.observable();
	self.bloco = ko.observable();
	self.beacon = ko.observable();
	
	self.descricaoCompleta = ko.computed(function() {
        return self.bloco() + " " + self.descricao();
    }, self);
}

function Evento(id, nome, descricao, data, ambiente) {
	var self = this;
	
	self.id = ko.observable(id);
	self.descricao = ko.observable(descricao);
	self.ambiente = ko.observable(ambiente);
	self.data = ko.observable(data);
	self.nome = ko.observable(nome);
}