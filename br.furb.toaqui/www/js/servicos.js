var GerenciadorAplicacao = new function(){
	var self = this;
	this.beacons;
	this.eventos;
	this.salas;
	this.parametros;
	this.mapa;
	
	this.avisos = {};
	this.cacheSalas = {};
	this.exibirConfiguracoes = false;
	
	var sintetizador;
	//var db = null;
	var ibeacons;
	
	/** INICIO BEACONS ***/
	
	this.iniciarMonitoramento = function(){
		for(var chave in this.beacons){
			var beacon = this.beacons[chave];
			ibeacons.monitorar(beacon.uid(), beacon.id());
		}
		ibeacons.iniciar(this.atualizarDistanciaBeacon);
		this.iniciarAtualizacaoEventosSalas();
	};
	
	this.buscarBeaconsIniciarMonitoramento = function(){
		db.transaction(function(tx){
			//adicionar os beacons em memória e cria os listeners
			tx.executeSql('SELECT * FROM BEACON WHERE VIRTUAL = "false"', [], function (tx,result){
				if (result.rows.length > 0) {
					for(var i=0; i<result.rows.length; i++) {
						var row = result.rows.item(i);
						var beaconViewModel = new Beacon(row['ID'], row['UID'], row['LATITUDE'], row['LONGITUDE'], row['ANDAR']);
						GerenciadorAplicacao.beacons[beaconViewModel.id()] = beaconViewModel;
					}
					try {
						GerenciadorAplicacao.iniciarMonitoramento();
					}catch(e){
						alert('a '+e.message);
					}
				}
			}, function(e){alert("erro a "+e.message);});
		}, errorCB);
	};
	
	this.buscarBeaconsVirtuaisProximos = function(tx, beacon, distancia){
		var distanciaMaxima = distancia * 1.40;
		var distanciaMinima = distancia - (distancia * 0.40);
		var beaconsVirtuais = [];
		tx.executeSql('SELECT B.* FROM BEACON B INNER JOIN PONTO_INTERMEDIARIO PI ON PI.BEACON_VIRTUAL = B.ID WHERE PI.BEACON_FISICO = '+ beacon +' AND PI.DISTANCIA >= '+distanciaMinima+' AND DISTANCIA <= '+ distanciaMaxima +' AND B.VIRTUAL = "true"', [], function (txt,result){
			if (result.rows.length > 0) {
				for(var i=0; i<result.rows.length; i++) {
					var row = result.rows.item(i);
					try {
						var beaconViewModel = new Beacon(row['ID'], row['UID'], row['LATITUDE'], row['LONGITUDE'], row['ANDAR']);
						beaconViewModel.virtual(true);
						beaconsVirtuais.push(beaconViewModel);
					}catch(e){
						alert('Erro ao adicionar beacon virtual ' + e.message);
					}
				}
				$.each(beaconsVirtuais, function(index, item){
					tx.executeSql('SELECT P.* FROM PONTO_INTERMEDIARIO P WHERE P.BEACON_VIRTUAL = ' + item.id(), [], function (txt,r){
						if (r.rows.length > 0) {
							for(var i=0; i < r.rows.length; i++) {
								var row = r.rows.item(i);
								var ponto = new PontoIntermediario();
								ponto.id(row["ID"]);
								ponto.beaconFisico(row["BEACON_FISICO"]);
								ponto.distancia(row["DISTANCIA"]);
								var beaconEncontrado = null;
								for(var chave in GerenciadorAplicacao.beacons){
									var beaconLoop = GerenciadorAplicacao.beacons[chave];
									if(beaconLoop.id() == ponto.beaconFisico() && beaconLoop.ultimaDistancia() * 1.40 >= ponto.distancia() && 
										beaconLoop.ultimaDistancia()-(beaconLoop.ultimaDistancia()*0.40) <= ponto.distancia()){
										beaconEncontrado = beaconLoop;
										break;
									}
								}
								if(beaconEncontrado != null){
									item.pontos.push(ponto);
								}
							}
							if(item.pontos().length >= 2){
								GerenciadorAplicacao.buscarAmbientesPorBeacon(tx, item.id(), 1, 1);
								GerenciadorAplicacao.buscarEventosPorBeacon(tx, item.id(), 1, 1);
								GerenciadorAplicacao.verificarPosicaoMapa(item.id(), distancia);
							} else {
								GerenciadorAplicacao.salas.removerPorBeacon(item.id());
								GerenciadorAplicacao.eventos.removerPorBeacon(item.id());
							}
						}
					}, function(e){alert("Erro de SQL Pontos Intermediários "+e.message);});
				}); 
			}
		}, function(e){alert("Erro de SQL Beacons Virtuais "+e.message);});
	};
	
	this.atualizarDistanciaBeacon = function(pluginResult){
		var jsonData = JSON.stringify(pluginResult);
		var jsonPData = JSON.parse(jsonData);
		
		var distancia = -1;
		$.each(jsonPData.beacons, function(index, item){
			distancia = item.accuracy;
			//alert(jsonPData.region.identifier + '-' + distancia);
		});
		var identificador = jsonPData.region.identifier;
		var beacon = GerenciadorAplicacao.beacons[identificador];
		if((GerenciadorAplicacao.salas.andar() == null || distancia <= 5) && distancia > -1)
			GerenciadorAplicacao.salas.andar(beacon.andar());
		
		if((GerenciadorAplicacao.avisos[identificador] != undefined && Math.abs(new Date() - GerenciadorAplicacao.avisos[identificador]) > 15000 && distancia == -1) || distancia > -1){
			beacon.ultimaDistancia(distancia);
		}
	};
	
	this.iniciarAtualizacaoEventosSalas = function(){
		setInterval(function(){
			db.transaction(function(tx){
				try {
					for(var chave in GerenciadorAplicacao.beacons){
						var beacon = GerenciadorAplicacao.beacons[chave];
						var distancia = beacon.andar() == GerenciadorAplicacao.salas.andar() ? beacon.ultimaDistancia() : -1;
						
						if(distancia > -1){
							//retorna os beacons virtuais existente na distancia determinada, com variação de +-20%
							GerenciadorAplicacao.buscarBeaconsVirtuaisProximos(tx, beacon.id(), beacon.ultimaDistancia());
						}
						var temBeacons = distancia != -1 ? 1 : 0;
						GerenciadorAplicacao.buscarAmbientesPorBeacon(tx, beacon.id(), temBeacons, distancia);
						GerenciadorAplicacao.buscarEventosPorBeacon(tx, beacon.id(), temBeacons, distancia);
						
						if(GerenciadorAplicacao.eventos.quantidadeEventosInteracao() > 0){
							cordova.plugins.notification.local.schedule({
								id: 1,
								title: "Interação FURB",
								text: GerenciadorAplicacao.eventos.quantidadeEventosInteracao() + " oficina(s)",
								//data: { meetingId:"123#fg8" }
							});
						} else {
							cordova.plugins.notification.local.cancel(1, function () {
							}, this);
						}
						if(GerenciadorAplicacao.eventos.quantidadeEventosNormais() > 0){
							cordova.plugins.notification.local.schedule({
								id: 2,
								title: "Eventos FURB",
								text: GerenciadorAplicacao.eventos.quantidadeEventosNormais() + " evento(s)",
								//data: { meetingId:"123#fg8" }
							});
						} else {
							cordova.plugins.notification.local.cancel(2, function () {
							}, this);
						}
					}
				}catch(e){alert(e.message);}
			}, errorCB);
		}, 3000);
	};
	
	this.procurarEventoAtualizar = function(evento, remover){
		if(remover){
			GerenciadorAplicacao.eventos.remover(evento);
		}
		else {
			GerenciadorAplicacao.eventos.adicionar(evento);
		}
		GerenciadorAplicacao.avisos[evento.id()] = new Date();
	};
	
	this.buscarAmbientesPorBeacon = function (tx, beacon, quantidade, distancia){
		tx.executeSql('SELECT A.ID AS AID, A.DESCRICAO AS ADESCRICAO, A.BEACON AS ABEACON, B.DESCRICAO AS BLOCO ' + 
								' FROM AMBIENTE A INNER JOIN BLOCO B ON B.ID = A.BLOCO WHERE A.BEACON = ' + beacon, [], function(txt, result){
			for(var i = 0; i < result.rows.length; i++) {
				try {
					if(distancia > -1){
						if(distancia > 25.0 || (quantidade == 0 && GerenciadorAplicacao.cacheSalas[beacon] != undefined && Math.abs(new Date() - GerenciadorAplicacao.cacheSalas[beacon]) > 25000)){
							GerenciadorAplicacao.salas.removerPorBeacon(beacon);
						} else {
							GerenciadorAplicacao.cacheSalas[beacon] = new Date();
							var row = result.rows.item(i);
							var a = new Ambiente();
							a.id(row["AID"]);
							a.descricao(row["ADESCRICAO"]);
							a.beacon(row["ABEACON"]);
							a.bloco(row["BLOCO"]);
							
							GerenciadorAplicacao.salas.adicionar(a, distancia);
						}
					} else {
						if(GerenciadorAplicacao.cacheSalas[beacon] != undefined && Math.abs(new Date() - GerenciadorAplicacao.cacheSalas[beacon]) > 25000){
							GerenciadorAplicacao.salas.removerPorBeacon(beacon);
						}
					}
				} catch(ex){
					alert("Erro ao adicionar ambiente. " + ex.message);
				}
			}
				
		}, function(ex){
			alert("Erro de SQL ao carregar ambientes. "+ex.message);
		});
	};
	
	this.buscarEventosPorBeacon = function(tx, beacon, quantidade, distancia){
		var agora = new Date();
		var agoraMaisUmaHora = new Date();
		agoraMaisUmaHora.setTime(agora.getTime() +  (60 * 60 * 1000));
		tx.executeSql('SELECT 	strftime("%m/%d/%Y %H:%M:%S", E.DATA_INICIO) AS DATAINICIO, strftime("%m/%d/%Y %H:%M:%S", E.DATA_FIM) AS DATAFIM, E.*, A.ID AS AID, A.DESCRICAO AS ADESCRICAO, A.BEACON AS ABEACON, B.DESCRICAO AS BLOCO ' + 
								' FROM EVENTO E INNER JOIN AMBIENTE A ON A.ID = E.AMBIENTE INNER JOIN BLOCO B ON B.ID = A.BLOCO ' + 
								'WHERE ((strftime("%s", datetime("'+agora.format("yyyy-mm-dd HH:MM:ss")+'")) BETWEEN strftime("%s", E.DATA_INICIO) AND '+
								'strftime("%s", E.DATA_FIM)) OR (strftime("%s", E.DATA_INICIO) BETWEEN strftime("%s", datetime("'+agora.format("yyyy-mm-dd HH:MM:ss")+'")) '+
								'AND strftime("%s", datetime("'+agoraMaisUmaHora.format("yyyy-mm-dd HH:MM:ss")+'")))) AND A.BEACON = ' + beacon, [], function(txt, result){
			for(var i = 0; i < result.rows.length; i++) {
				try {
					var row = result.rows.item(i);
					var e = new Evento(row["ID"], row["NOME"], row["DESCRICAO"], row["DATAINICIO"], row["DATAFIM"], row["AMBIENTE"], row["TIPO"]);
					if(distancia > -1){
						var a = new Ambiente();
						a.id(row["AID"]);
						a.descricao(row["ADESCRICAO"]);
						a.beacon(GerenciadorAplicacao.beacons[row["ABEACON"]]);
						a.bloco(row["BLOCO"]);
						e.ambiente(a);
						
						if((quantidade == 0 && GerenciadorAplicacao.avisos[e.id()] != undefined && Math.abs(new Date() - GerenciadorAplicacao.avisos[e.id()]) > 25000) || distancia > 10.0){
							GerenciadorAplicacao.procurarEventoAtualizar(e, true);
						}
						else {
							GerenciadorAplicacao.procurarEventoAtualizar(e, false);
						}
					} else {
						if(GerenciadorAplicacao.avisos[e.id()] != undefined && Math.abs(new Date() - GerenciadorAplicacao.avisos[e.id()]) > 25000){
							GerenciadorAplicacao.procurarEventoAtualizar(e, true);
						}
					}
				} catch(ex){
					alert("Erro ao atualizar os eventos. " + ex.message);
				}
			}
		}, function(ex){
			alert("Erro de SQL ao carregar eventos. "+ex.message);
		});
		this.verificarPosicaoMapa(beacon, distancia);
	};
	
	/** FIM BEACONS ***/
	
	/** INICIO SINTETIZADOR ***/
	
	this.falar = function(frase){
		sintetizador.falar(frase);
	};
	
	/** FIM SINTETIZADOR ***/
	
	this.iniciar = function(elementoMapa){
		sintetizador = new Sintetizador();
		sintetizador.iniciar();
		this.mapa = new Mapa();
		this.mapa.iniciar(elementoMapa);
		ibeacons = new IBeacons();
		this.beacons = {};
		this.eventos = new Eventos();
		this.salas =  new Salas();
		this.parametros = new Parametros();
		this.buscarBeaconsIniciarMonitoramento();
	};
	
	/** INICIO MAPA ***/
	
	this.verificarPosicaoMapa = function(beacon, distancia){
		if (!GerenciadorAplicacao.mapa.gpsAtivo && distancia > -1) {
			if (distancia < 3.0) { //TODO: verificar distancia a ser considerada
				db.transaction(function(tx){ 
					tx.executeSql('SELECT * FROM BEACON WHERE ID = '+ beacon +' AND VIRTUAL = "false"', [], function (txt,result){
						if (result.rows.length > 0) {
							var row = result.rows.item(0);
							var localizacao = new Localizacao(row['LATITUDE'], row['LONGITUDE']);
							GerenciadorAplicacao.atualizarPosicaoMapa(localizacao);
						}
					}, function(e){
						alert(e.message);
					});
				}, function(e){
					alert('Erro ao verificar posição geográfica do beacon.\r\n'+ e.message);
				});
			}
		}
	};
	
	this.adicionarPin = function(descricao, localizacao){
		this.mapa.adicionarPin(descricao, localizacao);
	};
	
	this.gpsAtivo = function(){
		return this.mapa.gpsAtivo;
	};
	
	this.atualizarPosicaoMapa = function(posicao){
		this.mapa.atualizarPosicaoMapa(posicao);
	};
	
	/** FIM MAPA ***/
	
	/** INICIO BANCO ***/
	
	this.iniciarBancoDados = function(){
		db = window.openDatabase("TOAQUI", "1.0", "TOAQUI", 65535);
		//inicialização do banco de dados padrão
		//db.transaction(populateDB, errorCB, successCB);
	};
	
	/** FIM BANCO ***/
}

var dateFormat = function () {
	var    token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var    _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};