function nullHandler(){}
			
//create table and insert some record
function populateDB(tx) {
	var dataEventoHoje = new Date();
	var dataEventoHojeFim = new Date();
	dataEventoHojeFim.setTime(dataEventoHoje.getTime() +  (1 * 60 * 60 * 1000));
	var dataEventoHoje1 = new Date();
	dataEventoHoje1.setTime(dataEventoHoje.getTime() +  (0.5 * 60 * 60 * 1000));
	var dataEventoHojeFim1 = new Date();
	dataEventoHojeFim1.setTime(dataEventoHoje1.getTime() +  (1 * 60 * 60 * 1000));
	var dataEventoHoje2 = new Date();
	dataEventoHoje2.setTime(dataEventoHoje1.getTime() +  (0.5 * 60 * 60 * 1000));
	var dataEventoHojeFim2 = new Date();
	dataEventoHojeFim2.setTime(dataEventoHoje2.getTime() +  (1 * 60 * 60 * 1000));
	
	
	var dataEventoAmanha = new Date();
	var dataEventoAmanhaFim = new Date();
	var duration = 1; //In Days
	dataEventoAmanha.setTime(dataEventoHoje.getTime() +  (duration * 24 * 60 * 60 * 1000));
	dataEventoAmanhaFim.setTime(dataEventoAmanha.getTime() +  (2 * 60 * 60 * 1000));
	
	tx.executeSql('CREATE TABLE IF NOT EXISTS BEACON (ID INTEGER PRIMARY KEY, UID TEXT NOT NULL, LATITUDE FLOAT NOT NULL, LONGITUDE FLOAT NOT NULL, ANDAR INT NOT NULL, VIRTUAL VARCHAR(5) NOT NULL)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS BLOCO (ID INTEGER PRIMARY KEY, DESCRICAO CHARACTER(1) NOT NULL)');
	tx.executeSql('CREATE TABLE IF NOT EXISTS PONTO_INTERMEDIARIO (ID INTEGER PRIMARY KEY, BEACON_VIRTUAL INT NOT NULL, BEACON_FISICO INT NOT NULL, DISTANCIA FLOAT NOT NULL, FOREIGN KEY (BEACON_VIRTUAL) REFERENCES BEACON(ID), FOREIGN KEY (BEACON_FISICO) REFERENCES BEACON(ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS AMBIENTE (ID INTEGER PRIMARY KEY, DESCRICAO TEXT NOT NULL, BLOCO INT NOT NULL, BEACON INT NOT NULL, FOREIGN KEY (BEACON) REFERENCES BEACON(ID), FOREIGN KEY (BLOCO) REFERENCES BLOCO(ID))');
	tx.executeSql('CREATE TABLE IF NOT EXISTS EVENTO (ID INTEGER PRIMARY KEY, TIPO INTEGER NOT NULL, NOME VARCHAR(150) NOT NULL, DESCRICAO TEXT NOT NULL, DATA_INICIO DATETIME NOT NULL, DATA_FIM DATETIME NOT NULL, AMBIENTE INTEGER NOT NULL, FOREIGN KEY (AMBIENTE) REFERENCES AMBIENTE(ID))');
	//tx.executeSql('CREATE TABLE IF NOT EXISTS PARAMETRO (ID INTEGER PRIMARY KEY, CHAVE TEXT NOT NULL, VALOR  VARCHAR(150) NOT NULL)');
	
	tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR, VIRTUAL) VALUES ("bec26202-a8d8-4a94-80fc-9ac1de37daa5", -26.905294, -49.078137, 1, "false")');
	tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR, VIRTUAL) VALUES ("bec26202-a8d8-4a94-80fc-9ac1de37daa6", -26.905286, -49.078255, 1, "false")');
	tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR, VIRTUAL) VALUES ("bec26202-a8d8-4a94-80fc-9ac1de37daa7", -26.905339, -49.078117, 2, "false")');
	tx.executeSql('INSERT INTO BEACON (UID, LATITUDE, LONGITUDE, ANDAR, VIRTUAL) VALUES ("BEACONS212", -26.905678, -49.079770, 2, "true")');

	tx.executeSql('INSERT INTO PONTO_INTERMEDIARIO (BEACON_VIRTUAL, BEACON_FISICO, DISTANCIA) VALUES (4, 2, 10)');
	tx.executeSql('INSERT INTO PONTO_INTERMEDIARIO (BEACON_VIRTUAL, BEACON_FISICO, DISTANCIA) VALUES (4, 3, 10)');
	
	tx.executeSql('INSERT INTO BLOCO (DESCRICAO) VALUES ("G")');
	tx.executeSql('INSERT INTO BLOCO (DESCRICAO) VALUES ("R")');
	
	//1º andar - bloco G
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("INFORMAÇÕES", 1, 2)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("001", 1, 2)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("003", 1, 2)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("002", 1, 2)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("004", 1, 2)');
	//2º andar - bloco G - lado escada
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("101", 1, 3)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("103", 1, 3)');
	//2º andar - bloco G - lado secretaria
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("100", 1, 3)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("102", 1, 3)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("104", 1, 3)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("106", 1, 3)');
	tx.executeSql('INSERT INTO AMBIENTE (DESCRICAO, BLOCO, BEACON) VALUES ("SECRETARIA", 1, 3)');
	
	//'2007-01-01 10:00:00'
	//tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Espanhol", "DESCRIÇÃO 1", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 5, 0)');
	//tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Palestra de Contabilidade", "DESCRIÇÃO 2", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 2, 1)');
	//tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Robótica", "DESCRIÇÃO 3", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+ dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 3, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Computação Gráfica", "DESCRIÇÃO 4", "'+dataEventoHoje1.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim1.format("yyyy-mm-dd HH:MM:ss")+'", 4, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Anatomia", "DESCRIÇÃO 5", "'+dataEventoHoje2.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim2.format("yyyy-mm-dd HH:MM:ss")+'", 11, 0)');
	
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Robótica", "DESCRIÇÃO 1", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", 9, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Palestra de Contabilidade", "DESCRIÇÃO 2", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 4, 1)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Espanhol", "DESCRIÇÃO 3", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+ dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss") +'", 6, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Computação Gráfica", "DESCRIÇÃO 4", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 7, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Anatomia", "DESCRIÇÃO 5", "'+dataEventoHoje.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim.format("yyyy-mm-dd HH:MM:ss")+'", 11, 0)');
	tx.executeSql('INSERT INTO EVENTO (NOME, DESCRICAO, DATA_INICIO, DATA_FIM, AMBIENTE, TIPO) VALUES ("Oficina de Matemática", "DESCRIÇÃO 6", "'+dataEventoHoje1.format("yyyy-mm-dd HH:MM:ss")+'", "'+dataEventoHojeFim1.format("yyyy-mm-dd HH:MM:ss")+'", 9, 0)');
	
	/*tx.executeSql('INSERT INTO PARAMETRO (CHAVE, VALOR) VALUES ("EXIBIR_INTERACAO", "true")');
	tx.executeSql('INSERT INTO PARAMETRO (CHAVE, VALOR) VALUES ("EXIBIR_NORMAIS", "true")');
	tx.executeSql('INSERT INTO PARAMETRO (CHAVE, VALOR) VALUES ("AUDIO", "true")');
	*/
	
	localStorage.setItem("interacaoFURB", "true");
	localStorage.setItem("normais", "true");
	localStorage.setItem("audio", "true");
		
	localStorage.setItem("bancoInicializado", true);
}

//function will be called when an error occurred
function errorCB(err) {
	alert("ERRO DE SQL: " + err.message + " - " + err.code);
}

//function will be called when process succeed
function successCB() {
	alert("SUCESSO, BANCO INICIALIZADO");
	//db.transaction(buscarBeaconsIniciarMonitoramento,errorCB);
	iniciarAplicativo();
}