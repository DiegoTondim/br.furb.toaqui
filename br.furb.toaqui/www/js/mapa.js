function Mapa(){
	var self = this;
	var map;
	var marker;
	var pin;
	this.gpsAtivo = false;
	
	this.iniciar = function(elemento){
		self.map = plugin.google.maps.Map.getMap($(elemento)[0]);
		self.map.addEventListener(plugin.google.maps.event.MAP_READY, self.mapaCarregado);
	};
	
	this.mapaCarregado = function () {
		self.map.setIndoorEnabled(true);
		var onSucesso = function(location) {
			GerenciadorAplicacao.mapa.gpsAtivo = true;
			self.atualizarPosicaoMapa(location);
		};
		var onErro = function(msg) {
			GerenciadorAplicacao.mapa.gpsAtivo = false;
		};

		setInterval(function(){ 
			try {
				self.map.getMyLocation(onSucesso, onErro);
			}catch(e){alert(e.message);}
		}, 8000);
	};
	
	this.adicionarPin = function(descricao, localizacao){
		if(self.pin != null)
			self.pin.remove();
		var position = new plugin.google.maps.LatLng(localizacao.latLng.lat, localizacao.latLng.lng);
		self.map.addMarker({
		  position: position,
		  'title': descricao,
		  icon: 'blue',
		  animation: plugin.google.maps.Animation.DROP
		}, function(pinCriado) {
			self.pin = pinCriado;
			self.map.moveCamera({
				'target': position,
				'zoom': 18,
				}, function() {
					self.pin.showInfoWindow();
			});
		  
			setTimeout(function() {
				self.pin.remove();
				self.pin = null;
			}, 25000);
		});
	};
	
	this.atualizarPosicaoMapa = function (localizacao)
	{
		if (localizacao == null)
			return;
		self.map.addMarker({
			'position': localizacao.latLng,
			'title': "Você está aqui!"
		}, function(m) {
			if(marker)
				marker.remove();
			marker = m;
			marker.showInfoWindow();
		});
		//var position = new plugin.google.maps.LatLng(localizacao.latLng.lat, localizacao.latLng.lng);
		self.map.moveCamera({
			'target': localizacao.latLng,
			'zoom': 18
		}, function() {});
	};
}