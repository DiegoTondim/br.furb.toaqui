EventoTipo = Object.freeze({'interacaoFURB':0, 'normal':1});

function Parametros(){
	var self = this;
	
	self.interacaoFURB = ko.observable(true).extend({
        booleanValue: null
    });
	self.normais = ko.observable(true).extend({
        booleanValue: null
    });
	self.audio = ko.observable();
	
	self.interacaoFURB.subscribe(function(value){
		if(!value && !self.normais())
		{
			alert("Você deve ter pelo menos um filtro ativo");
			self.interacaoFURB(true);
		}
		else
			atualizarParametros(GerenciadorAplicacao.parametros);//db.transaction(function(tx){  }, errorCB);
	});
	self.normais.subscribe(function(value){
		if(!value && !self.interacaoFURB())
		{
			alert("Você deve ter pelo menos um filtro ativo");
			self.normais(true);
		}
		else
			atualizarParametros(GerenciadorAplicacao.parametros);//db.transaction(function(tx){ , tx); }, errorCB);
	});
}

function Eventos() {
	var self = this;
	
	self.eventos = ko.observableArray([]);
	
	self.exibirEvento = function(evento){
		var beacon = GerenciadorAplicacao.beacons[evento.ambiente().beacon().id()];
		var localizacao = new Localizacao(beacon.latitude(), beacon.longitude());
		GerenciadorAplicacao.adicionarPin(evento.nome() + '\n' + evento.ambiente().descricaoCompleta(), localizacao);
		if(GerenciadorAplicacao.parametros.audio())
			GerenciadorAplicacao.falar(evento.informacoes());
	};
	
	self.resultados = ko.computed(function () {
		var lista = self.eventos();
		var filteredCollection = ko.utils.arrayFilter(lista, function (item) {
			return (item.tipo() == EventoTipo.interacaoFURB && GerenciadorAplicacao.parametros.interacaoFURB()) || (item.tipo() == EventoTipo.normal && GerenciadorAplicacao.parametros.normais());
		});
		return filteredCollection;
	});
	
	self.eventosEmExecucao = function () {
		var filteredCollection = ko.utils.arrayFilter(self.resultados(), function (item) {
			return item.isExecutando();
		});
		return filteredCollection;
	};
	
	self.eventosFuturos = function () {
		var filteredCollection = ko.utils.arrayFilter(self.resultados(), function (item) {
			return !item.isExecutando();
		});
		return filteredCollection;
	};
	
	self.obter = function(index){
		return self.resultados()[index];
	};
	
	self.adicionar = function(evento){
		existe = false;
		for(i = 0; i < self.quantidadeEventos(); i++){
			if(self.eventos()[i].id() == evento.id()){
				existe = true;
				break;
			}
		}
		if(!existe)
			self.eventos.push(evento);
	};
	self.remover = function(evento){
		self.eventos.remove(function(item) { 
			return item.id() == evento.id();
		});
	};
	
	self.removerPorBeacon = function(beacon){
		self.eventos.remove(function(item) { 
			return item.ambiente().beacon() == beacon;
		});
	};
	
	self.quantidadeEventosNormais = ko.computed(function () {
		var filteredCollection = ko.utils.arrayFilter(self.eventos(), function (item) {
			return !item.isInteracaoFURB() && GerenciadorAplicacao.parametros.normais();
		});
		return filteredCollection.length;
	}, self);
	
	self.quantidadeEventosInteracao = ko.computed(function () {
		var filteredCollection = ko.utils.arrayFilter(self.eventos(), function (item) {
			return item.isInteracaoFURB() && GerenciadorAplicacao.parametros.interacaoFURB();
		});
		return filteredCollection.length;
	}, self);
	
	self.quantidadeEventos = function(){
		return self.eventos().length;
	};
	
	self.quantidade = function(){
		return self.resultados().length;
	};
}

function Salas(eventos) {
	var self = this;
	
	self.salas = ko.observableArray([]);
	self.bloco = ko.observable();
	self.andar = ko.observable(null);
	
	self.bloco.subscribe(function(value){
        self.nomeBloco();
	});
	
	self.andar.subscribe(function(value){
        self.nomeBloco();
    });

	self.exibirSala = function(sala){
		var beacon = GerenciadorAplicacao.beacons[sala.beacon()];
		var localizacao = new Localizacao(beacon.latitude(), beacon.longitude());
		GerenciadorAplicacao.adicionarPin(sala.descricaoCompleta(), localizacao);
		if(GerenciadorAplicacao.parametros.audio())
			GerenciadorAplicacao.falar(sala.informacoes());
	};
	
    self.nomeBloco = function() {
        if (typeof self.bloco() != 'undefined' && typeof self.andar() != 'undefined')
            $('#bloco').html(self.bloco() +' - '+ self.andar() + 'º ANDAR');
        else if (typeof self.bloco() != 'undefined')
            $('#bloco').html(self.bloco());
        else if(typeof self.andar() != 'undefined')
            $('#bloco').html(self.andar()+ 'º ANDAR')
		else
			$('#bloco').html('');
    };

	self.obter = function(index){
		return self.salas()[index];
	};
	self.adicionar = function(sala, distancia){
		if(distancia <= 8.0){
			self.bloco("VOCÊ ESTÁ NO BLOCO " + sala.bloco());
		}
		existe = false;
		for(i = 0; i < self.quantidade(); i++){
			if(self.salas()[i].id() == sala.id()){
				existe = true;
				break;
			}
		}
		if(!existe)
			self.salas.push(sala);
	};
	self.removerPorBeacon = function(beacon){
		self.salas.remove(function(item) { 
			return item.beacon() == beacon;
		});
	};
	self.remover = function(sala){
		if(self.quantidade() == 0)
			self.bloco("");
			
		self.salas.remove(function(item) { 
			return item.id() == sala.id();
		});
	};
	self.quantidade = function(){
		return self.salas().length;
	};
}

function Beacon(id, uid, latitude, longitude, andar) {
	var self = this;
	
	self.id = ko.observable(""+id);
	self.uid = ko.observable(uid);
	self.latitude = ko.observable(latitude);
	self.longitude = ko.observable(longitude);
	self.andar = ko.observable(andar);
	self.virtual = ko.observable(false);
	self.pontos = ko.observableArray([]);
	self.ultimaDistancia = ko.observable(-1);
}

function PontoIntermediario(){
	var self = this;
	
	self.id = ko.observable();
	self.beaconFisico = ko.observable();
	self.distancia = ko.observable();
}

function Ambiente() {
	var self = this;
	
	self.id = ko.observable();
	self.descricao = ko.observable();
	self.bloco = ko.observable();
	self.beacon = ko.observable();
	
	self.descricaoCompleta = ko.computed(function() {
        return self.bloco() + " " + self.descricao();
    }, self);
	
	self.informacoes = function(){
		var mensagem =  '';
		var distancia = GerenciadorAplicacao.beacons[self.beacon()].ultimaDistancia();
		
		if(distancia < 2 && distancia != -1)
			mensagem = 'Você está próximo de ' + self.descricaoCompleta();
		else if(distancia >= 2)
			mensagem = 'Você está à aproximadamente '+ distancia +' metros de ' + self.descricaoCompleta();
		else 
			mensagem = self.descricaoCompleta();
		
		return mensagem;
	};
}

function Evento(id, nome, descricao, dataInicio, dataFim, ambiente, tipo) {
	var self = this;
	
	self.id = ko.observable(id);
	self.descricao = ko.observable(descricao);
	self.ambiente = ko.observable(ambiente);
	self.dataInicio = ko.observable(new Date(dataInicio));
	self.dataFim = ko.observable(new Date(dataFim));
	self.nome = ko.observable(nome);
	self.tipo = ko.observable(tipo);
	self.ultimoAvisoSonoro = ko.observable(null);

	self.isExecutando = function(){
		var agora = new Date().getTime();
		
		return agora >= self.dataInicio().getTime() && agora <= self.dataFim().getTime();
	};

	self.informacoes = function(){
		var mensagem = self.nome() + ' ';
		
		if(self.isExecutando())
			mensagem += ' iniciou às '+ self.dataInicio().format('HH') + ' e '+ self.dataInicio().format('MM');
		else
			mensagem += ' terá início às '+ self.dataInicio().format('HH') + ' e '+ self.dataInicio().format('MM');
			
		mensagem += ' em ' + self.ambiente().descricaoCompleta();
		
		return mensagem;
	};
	
	self.isInteracaoFURB = ko.computed(function(){
		return self.tipo() == EventoTipo.interacaoFURB;
	}, self);
}

function Localizacao(latitude, longitude) {
    var self = this;
    
    self.latLng = new Coordenada(latitude, longitude);
}

function Coordenada(latitude, longitude) {
    var self = this;

    self.lat = latitude;
    self.lng = longitude;
}

ko.extenders.booleanValue = function (target) {
    target.formattedValue = ko.computed({
        read: function () {
            if (target() === true) return "true";
            else if (target() === false) return "false";
        },
        write: function (newValue) {
            if (newValue) {
                if (newValue === "false") target(false);
                else if (newValue === "true") target(true);
            }
        }
    });

    target.formattedValue(target());
    return target;
};
		
ko.bindingHandlers.jqmFlip = {
    init: function (element, valueAccessor) {
        var result = ko.bindingHandlers.value.init.apply(this, arguments);
        try {
            $(element).slider("refresh");
        } catch (x) {}
        return result;
    },
    update: function (element, valueAccessor) {
        ko.bindingHandlers.value.update.apply(this, arguments);
        var value = valueAccessor();
        var valueUnwrapped = ko.utils.unwrapObservable(value);
        try {
            $(element).slider("refresh");
        } catch (x) {}
    }
};