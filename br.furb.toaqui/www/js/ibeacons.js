function IBeacons(){
	
	this.criarBeacon = function(uuid, identifier){
		return new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, 0, 0);
	};

	this.monitorar = function(uid, identificador){
		cordova.plugins.locationManager.startRangingBeaconsInRegion(this.criarBeacon(uid, identificador)).done();
	}; 
	
	this.iniciar = function(callback){
		cordova.plugins.locationManager.requestWhenInUseAuthorization();
		var delegate = new cordova.plugins.locationManager.Delegate();
		
		delegate.didDetermineStateForRegion = function (pluginResult) {};
		
		delegate.didStartMonitoringForRegion = function (pluginResult) {};
			
		delegate.didRangeBeaconsInRegion = callback;
		
		cordova.plugins.locationManager.setDelegate(delegate);
	};
}