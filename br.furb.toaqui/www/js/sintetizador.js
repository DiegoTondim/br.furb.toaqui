function Sintetizador(){
	this.iniciar = function(){
		ttsPlugin.initTTS(function(){
			ttsPlugin.setRate("0.2"); // Set voice speed : default is "0.2"
			ttsPlugin.setLanguage("pt-BR"); // Set voice language : default is "en-US"
		}, function(){
			alert("Não foi possível iniciar o plugin de síntese de voz.\r\n");
		});
	}
	
	this.falar = function(frase){
		ttsPlugin.speak(frase);
		ttsPlugin.stopSpeaking();
	}
}