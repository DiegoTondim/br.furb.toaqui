$(function(){			
	app.initialize();
	
	document.addEventListener("deviceready", onDeviceReady, false);
});

$(document).on( "pagecreate", "#demo-page", function() {
    $(document).on("swipeleft swiperight", "#demo-page", function(e) {
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swipeleft") {
                $("#right-panel").panel("open");
            } else if (e.type === "swiperight") {
                $("#left-panel").panel("open");
            }
        }
    });
});

function onDeviceReady() {
	$("ul.lista").css("height", ($(window).height()-30)+"px");
	cordova.plugins.notification.local.on("click", function (notification) {
		$( "#left-panel" ).panel( "open" );
	});
	db = window.openDatabase("TOAQUI", "1.0", "TOAQUI", 200000);
	//inicialização do banco de dados padrão
	if(!window.localStorage.getItem("bancoInicializado"))
		db.transaction(populateDB, errorCB, successCB);
	else
		iniciarAplicativo();
}

function iniciarAplicativo(){
	$("#btn-audio").click(function(){
		GerenciadorAplicacao.parametros.audio(!GerenciadorAplicacao.parametros.audio());
		carregarParametroAudio();
		atualizarParametros(GerenciadorAplicacao.parametros);
		//db.transaction(function(tx){ atualizarParametros(GerenciadorAplicacao.parametros, tx); }, errorCB);
	});

	$("#btn-configuracoes").click(function(){
		GerenciadorAplicacao.exibirConfiguracoes = !GerenciadorAplicacao.exibirConfiguracoes;
		configurarPainelEsquerda();
	});	
	
	$('#lista-eventos').listview();
	
	//window.bluetooth.isEnabled(isEnabledSuccess, isEnabledError);
	
	try {
		GerenciadorAplicacao.iniciar("#map_canvas");
	}catch(e){
		alert('Erro ao inicializar aplicação!\r\n'+e.message);
	}
	
	try {
		$("#parametros").hide();
		$("#eventos").hide();
		configurarPainelEsquerda();
		ko.applyBindings(GerenciadorAplicacao.eventos, $("#eventos")[0]);
		ko.applyBindings(GerenciadorAplicacao.salas, $("#right-panel")[0]);
		obterParametros();
		//db.transaction(obterParametros, errorCB);
	}catch(e){
		alert("Erro ao aplicar binding na tela.\r\n"+e.message);
	}
	
	
	setInterval(function(){
		try {
			if(GerenciadorAplicacao.parametros.audio()){
				var mensagem = "";

				var eventosAviso = new Eventos();
				var eventosAvisoFuturo = new Eventos();
				
				if(GerenciadorAplicacao.salas.quantidade() > 0){
					mensagem = GerenciadorAplicacao.salas.bloco()+" "+GerenciadorAplicacao.salas.andar()+"º andar.";
				}
				
				var emExecucao = GerenciadorAplicacao.eventos.eventosEmExecucao();
				mensagem = montarMensagemVozEmExecucao(emExecucao, emExecucao.length, mensagem, eventosAviso);	
				
				var futuros = GerenciadorAplicacao.eventos.eventosFuturos();
				mensagem = montarMensagemVozFuturo(futuros, futuros.length, mensagem, eventosAvisoFuturo);
				
				if (mensagem !== "")
				{
					GerenciadorAplicacao.falar(mensagem);
				}
			}
		} catch(e){
			alert("Erro ao executar a síntese de voz. \r\n " + e.message);
		}
	}, 60000);
}

function carregarParametroAudio(){
	var btn = $("#btn-audio");
	if(GerenciadorAplicacao.parametros.audio()){
		btn.removeClass("inativo");
		btn.addClass("ativo");
	} else {
		btn.removeClass("ativo");
		btn.addClass("inativo");
	}
}

function montarMensagemVozEmExecucao(eventos, quantidade, mensagem, eventosAviso){
	if(quantidade == 0)
		return mensagem;
		
	for (i = 0; i < quantidade; i++){
		var e = eventos[i];
		if (e.ultimoAvisoSonoro() != null && Math.abs(new Date() - e.ultimoAvisoSonoro()) < 120000)
			continue;
		e.ultimoAvisoSonoro(new Date());
		eventosAviso.adicionar(e);
	}

	for(i = 0; i < eventosAviso.quantidade(); i++){
		var virgula = "";
		if(eventosAviso.quantidade() > 1 && i+2 < eventosAviso.quantidade())				
			virgula = ",";
		else if(eventosAviso.quantidade() > 1 && i+2 == eventosAviso.quantidade())
			virgula = " e ";
		
		var e = eventosAviso.obter(i);

		mensagem += e.nome() + virgula;
		
		if(eventosAviso.quantidade() == 1){
			mensagem += " está sendo realizado em ";
			mensagem +=  e.ambiente().bloco() + " " + e.ambiente().descricao() + ' ';
		}
		GerenciadorAplicacao.avisos[e.id()] = new Date();
	}
	if(eventosAviso.quantidade() > 1){
		mensagem += " estão sendo realizados próximos à você ";
	}
	return mensagem;
}

function montarMensagemVozFuturo(eventos, quantidade, mensagem, eventosAviso){
	if(quantidade == 0)
		return mensagem;
		
	for (i = 0; i < quantidade; i++){
		var e = eventos[i];
		if (e.ultimoAvisoSonoro() != null && Math.abs(new Date() - e.ultimoAvisoSonoro()) < 120000)
			continue;
		e.ultimoAvisoSonoro(new Date());
		eventosAviso.adicionar(e);
	}

	for(i = 0; i < eventosAviso.quantidade(); i++){
		var virgula = "";
		if(eventosAviso.quantidade() > 1 && i+2 < eventosAviso.quantidade())				
			virgula = ",";
		else if(eventosAviso.quantidade() > 1 && i+2 == eventosAviso.quantidade())
			virgula = " e ";
		
		var e = eventosAviso.obter(i);

		mensagem += e.nome() + virgula;
		
		if(eventosAviso.quantidade() == 1){
			mensagem += " será realizado em ";
			mensagem +=  e.ambiente().bloco() + " " + e.ambiente().descricao() + ' ';
		}
		GerenciadorAplicacao.avisos[e.id()] = new Date();
	}
	if(eventosAviso.quantidade() > 1){
		mensagem += "  serão realizados próximos à você ";
	}
	return mensagem;
}

function configurarPainelEsquerda(){
	if(GerenciadorAplicacao.exibirConfiguracoes){
		$("#parametros").toggle('slide');
		$("#eventos").hide();
	} else {
		$("#parametros").hide();
		$("#eventos").toggle('slide');
	}
}

function obterParametros(){
    /*
	tx.executeSql('SELECT * FROM PARAMETRO', [], function (txt,result){
		if (result.rows.length > 0) {
			GerenciadorAplicacao.parametros = new Parametros();
			for(var i=0; i < result.rows.length; i++) {
				var row = result.rows.item(i);
				if(row['CHAVE'] == "EXIBIR_INTERACAO"){
					GerenciadorAplicacao.parametros.interacaoFURB(row['VALOR'] == "true");
				}
				else if(row['CHAVE'] == "EXIBIR_NORMAIS"){
					GerenciadorAplicacao.parametros.normais(row['VALOR'] == "true");
				}
				else if(row['CHAVE'] == "AUDIO"){
					GerenciadorAplicacao.parametros.audio(row['VALOR'] == "true");
				}
			}	
		}
		ko.applyBindings(GerenciadorAplicacao.parametros, $("#parametros")[0]);
		carregarParametroAudio();
	}, function(e){
		alert("Erro ao carregar os parâmetros da aplicação.\r\n" + e.message);
	});*/
	var interacao = window.localStorage.getItem("interacaoFURB") == "true";
	var normais = window.localStorage.getItem("normais") == "true";
	var audio = window.localStorage.getItem("audio") == "true";
	
	GerenciadorAplicacao.parametros = new Parametros();
	GerenciadorAplicacao.parametros.interacaoFURB(interacao);
	//alert(localStorage.interacaoFURB);
	//alert(GerenciadorAplicacao.parametros.interacaoFURB());
	GerenciadorAplicacao.parametros.normais(normais);
	//alert(localStorage.normais);
	//alert(GerenciadorAplicacao.parametros.normais());
	GerenciadorAplicacao.parametros.audio(audio);
	//alert(localStorage.audio);
	//alert(GerenciadorAplicacao.parametros.audio());
	ko.applyBindings(GerenciadorAplicacao.parametros, $("#parametros")[0]);
	carregarParametroAudio();
}

function atualizarParametros(parametros){
	/*tx.executeSql('UPDATE PARAMETRO SET VALOR = ? WHERE CHAVE = ?', [GerenciadorAplicacao.parametros.interacaoFURB(), "EXIBIR_INTERACAO"], function SuccessUpdate(t,result){
		if (result.rowsAffected < 1){
			
		}
	}, function(e){alert("Erro ao atualizar os parâmetros.\r\n"+e.message);});
	tx.executeSql('UPDATE PARAMETRO SET VALOR = ? WHERE CHAVE = ?', [GerenciadorAplicacao.parametros.normais(), "EXIBIR_NORMAIS"], function SuccessUpdate(txt,result){
		if (result.rowsAffected < 1){
			
		}
	}, function(e){alert("Erro ao atualizar os parâmetros.\r\n"+e.message);});
	tx.executeSql('UPDATE PARAMETRO SET VALOR = ? WHERE CHAVE = ?', [GerenciadorAplicacao.parametros.audio(), "AUDIO"], function SuccessUpdate(txt,result){
		if (result.rowsAffected < 1){
			
		}
	}, function(e){alert("Erro ao atualizar os parâmetros.\r\n"+e.message);});*/
	
	localStorage.setItem("interacaoFURB", GerenciadorAplicacao.parametros.interacaoFURB());
	localStorage.setItem("normais", GerenciadorAplicacao.parametros.normais());
	localStorage.setItem("audio", GerenciadorAplicacao.parametros.audio());
}

function logToDom(message) {
	var e = document.createElement('label');
	e.innerText = message;
	var br = document.createElement('br');
	var br2 = document.createElement('br');
	document.body.appendChild(e);
	document.body.appendChild(br);
	document.body.appendChild(br2);
}

function isEnabledSuccess(isEnabled){
   if(!isEnabled)
	 enableBluetooth();
}

function isEnabledError(error){
}

function enableBluetooth(){
	window.bluetooth.enable( function(){}, function(){});
}